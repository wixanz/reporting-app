# ReportGenerator

The application provide the following functionality:

 - generate and save reports to db
 - show all reports from db
   
Additional requirements to the application:

 - reconnect to db every 5 seconds if connection was lost
 - save all generated reports during lost db connection asap
 - reports should be saved to db in ascending order without using any sorting algorithms 
 - the application have not to be overloaded due to report generation, slow db connection or in case db is too busy at the moment

Java technology stack:

 - Java 8 
 - Spring Boot JPA with Hibernate
 
Database:

  - MySQL



Usage:

Run report generation process:

```
java -jar ReportGenerator-1.0.jar 
```

Show all generated reports:

```
java -jar ReportGenerator-1.0.jar -p
```