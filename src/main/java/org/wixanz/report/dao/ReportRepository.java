package org.wixanz.report.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.wixanz.report.domain.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
}


