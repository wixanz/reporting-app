package org.wixanz.report;

import org.wixanz.report.service.ReportService;

public class ReportProducer implements Runnable {

  private ReportService reportService;

  public ReportProducer(ReportService reportService) {
    this.reportService = reportService;
  }

  public void run() {
    while (true) {
      try {
        reportService.generateReport();
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }
  }
}