package org.wixanz.report;

import java.lang.invoke.MethodHandles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.CannotCreateTransactionException;

import org.wixanz.report.service.ReportService;

public class ReportConsumer implements Runnable {

  private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
  private ReportService reportService;

  public ReportConsumer(ReportService reportService) {
    this.reportService = reportService;
  }

  public void run() {
    while (true) {
      try {
        reportService.saveReport();
      } catch (CannotCreateTransactionException e) {
        LOGGER.info("Connection lost");

        try {
          LOGGER.info("Repeat insert operation after 5 seconds");
          Thread.sleep(5000);
        } catch (InterruptedException ignored) {
        }
      }
    }
  }
}
