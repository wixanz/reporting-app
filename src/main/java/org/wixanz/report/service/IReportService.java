package org.wixanz.report.service;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.wixanz.report.domain.Report;

public interface IReportService {

  void runReportsGeneration();

  List<Report> listReports();

  BlockingQueue<Report> listReportsQueue();

  void clearReportsQueue();

}
