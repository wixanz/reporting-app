package org.wixanz.report.service;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.wixanz.report.ReportConsumer;
import org.wixanz.report.ReportProducer;
import org.wixanz.report.dao.ReportRepository;
import org.wixanz.report.domain.Report;

@Service
public class ReportService implements IReportService {

  private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  @Autowired
  private ReportRepository reportRepository;
  private BlockingQueue<Report> queue = new LinkedBlockingQueue<>();

  @Override
  public void runReportsGeneration() {

    Thread reportProducerThread = new Thread(new ReportProducer(this));
    Thread reportConsumerThread = new Thread(new ReportConsumer(this));

    Runtime.getRuntime().addShutdownHook(reportProducerThread);
    Runtime.getRuntime().addShutdownHook(reportConsumerThread);

    reportProducerThread.start();
    reportConsumerThread.start();
  }

  @Override
  public List<Report> listReports() {
    return reportRepository.findAll();
  }

  @Override
  public BlockingQueue<Report> listReportsQueue() {
    return queue;
  }

  @Override
  public void clearReportsQueue() {
    queue.clear();
  }

  public void saveReport() {
    Report report = queue.peek();
    if (report != null) {
      report = reportRepository.save(queue.peek());

      queue.poll();

      LOGGER.info(Thread.currentThread().getName() + ": Saved report['" + report.getTimestamp() + "']");
    }
  }

  public void generateReport() throws InterruptedException {
    Report report = new Report();
    queue.put(report);
    LOGGER.info(Thread.currentThread().getName() + ": Generated report[" + report.getTimestamp() + "']");
    LOGGER.info("Consumer queue size: " + queue.size());
  }
}
