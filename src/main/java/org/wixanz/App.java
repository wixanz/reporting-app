package org.wixanz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;
import org.wixanz.report.domain.Report;
import org.wixanz.report.service.ReportService;

import java.lang.invoke.MethodHandles;

@SpringBootApplication
@Profile("!test")
public class App implements CommandLineRunner {

  private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  @Autowired
  private ReportService reportService;

  public static void main(String[] args) {
    new SpringApplication(App.class).run(args);
  }

  @Override
  public void run(String... args) throws Exception {

    if (args.length == 0) {
      LOGGER.info("Reports generation process start...");
      reportService.runReportsGeneration();
    }

    if (args.length > 0 && args[0].equals("-p")) {
      for (Report report : reportService.listReports()) {
        LOGGER.info(report.getId() + " : " + report.getTimestamp());
      }
    }
  }
}
