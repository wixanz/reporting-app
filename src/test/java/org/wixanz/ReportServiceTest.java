package org.wixanz;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.wixanz.report.dao.ReportRepository;
import org.wixanz.report.domain.Report;
import org.wixanz.report.service.ReportService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppTest.class)
@ActiveProfiles("test")
public class ReportServiceTest {

  @Autowired
  private ReportRepository reportRepository;

  @Autowired
  private ReportService reportService;

  @Test
  public void testShowReports() {
    List<Report> expectedReports = new ArrayList<>(3);
    for (int i = 0; i < 3; i++) {
      expectedReports.add(reportRepository.save(new Report()));
    }

    List<Report> actualReports = reportService.listReports();
    assertEquals(expectedReports.size(), actualReports.size());

    reportRepository.deleteAll();
  }

  @Test
  public void testGenerateReport() throws InterruptedException {
    reportService.generateReport();

    BlockingQueue<Report> actualReportsQueue = reportService.listReportsQueue();
    assertEquals(1, actualReportsQueue.size());

    reportService.clearReportsQueue();
  }

  @Test
  public void testSaveReport() throws InterruptedException {
    reportService.generateReport();
    reportService.saveReport();

    BlockingQueue<Report> actualReportsQueue = reportService.listReportsQueue();
    assertEquals(0, actualReportsQueue.size());

    List<Report> actualReports = reportService.listReports();
    assertEquals(1, actualReports.size());

    reportService.clearReportsQueue();
    reportRepository.deleteAll();
  }
}
