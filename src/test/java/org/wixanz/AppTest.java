package org.wixanz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@Profile("test")
public class AppTest {

  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(AppTest.class);
    app.setLogStartupInfo(false);
    app.run(args);
  }
}
